export FSC ?= fsc
export TARGET ?= Debug

DBGFLAG =
ifeq ($(TARGET),Debug)
        DBGFLAG = --debug
endif

OUTDIR = $(CURDIR)/bin/$(TARGET)
#export MONO_PATH := $(MONO_PATH):$(OUTDIR)

.PHONY: all clean

all: $(OUTDIR)/adfquick.dll $(OUTDIR)/adftest.dll $(OUTDIR)/f6stest.exe

$(OUTDIR):
	mkdir -p $(OUTDIR)

#
# binary dependencies
#

#NDESK_OPTIONS = $(shell pkg-config --variable=Libraries ndesk-options)
#NDesk.Options: $(OUTDIR)/NDesk.Options.dll
$(OUTDIR)/NDesk.Options.dll: $(OUTDIR) bindeps/NDesk.Options.dll
	cp bindeps/NDesk.Options.dll $(OUTDIR)/

#cvodes: $(OUTDIR) $(OUTDIR)/cvodes.dll
$(OUTDIR)/cvodes.dll: $(OUTDIR) bindeps/cvodes.dll
	cp bindeps/cvodes.dll* $(OUTDIR)/

#knitro: $(OUTDIR) $(OUTDIR)/knitro.dll
$(OUTDIR)/knitro.dll: $(OUTDIR) bindeps/knitro.dll
	cp bindeps/knitro.dll* $(OUTDIR)/


#
# adfquick library
#

SRC_adfquick = src/bfloat.fs

REF_adfquick = cvodes.dll

RREF_adfquick = $(foreach I,$(REF_adfquick),-r:$(I))

DEP_adfquick = $(foreach I,$(REF_adfquick),$(OUTDIR)/$(I))

$(OUTDIR)/adfquick.dll: $(OUTDIR) $(SRC_adfquick) $(DEP_adfquick)
	$(FSC) $(DBGFLAG) --target:library --out:$(OUTDIR)/adfquick.dll -I:$(OUTDIR) $(RREF_adfquick) $(SRC_adfquick)
	chmod a+x $(OUTDIR)/adfquick.dll


#
# adftest unit tests
#

SRC_adftest = test/adftest.fs

SYS_adftest = nunit.framework.dll
REF_adftest = adfquick.dll

RREF_adftest = $(foreach I,$(REF_adftest),-r:$(I)) $(foreach I,$(SYS_adftest),-r:$(I))

DEP_adftest = $(foreach I,$(REF_adftest),$(OUTDIR)/$(I))

$(OUTDIR)/adftest.dll: $(OUTDIR) $(SRC_adftest) $(DEP_adftest)
	$(FSC) $(DBGFLAG) --target:library --out:$(OUTDIR)/adftest.dll -I:$(OUTDIR) $(RREF_adftest) $(SRC_adftest)
	chmod a+x $(OUTDIR)/adftest.dll


#
# f6stest program
#

SRC_f6stest = \
      f6stest/csv.fs \
      f6stest/evmodel.fs \
      f6stest/befit.fs \
      f6stest/berun.fs
#      f6stest/evmodel5.fs \
#      f6stest/evmodel5b.fs \
#      f6stest/evmodel5c.fs \
#      f6stest/evmodel6.fs \

SYS_f6stest = System.Core.dll
REF_f6stest = adfquick.dll cvodes.dll knitro.dll NDesk.Options.dll

RREF_f6stest = $(foreach I,$(REF_f6stest),-r:$(I)) $(foreach I,$(SYS_f6stest),-r:$(I))

DEP_f6stest = $(foreach I,$(REF_f6stest),$(OUTDIR)/$(I))

$(OUTDIR)/f6stest.exe: $(OUTDIR) $(SRC_f6stest) $(DEP_f6stest)
	$(FSC) $(DBGFLAG) --target:exe --out:$(OUTDIR)/f6stest.exe -I:$(OUTDIR) $(RREF_f6stest) $(SRC_f6stest)
	chmod a+x $(OUTDIR)/f6stest.exe

f6stest: adfquick cvodes knitro NDesk.Options
	make -C f6stest

clean:
	rm -f $(OUTDIR)/*.dll $(OUTDIR)/*.exe $(OUTDIR)/*.mdb
