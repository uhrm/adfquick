
module adftest

    open NUnit.Framework
    open adfquick

    [<TestFixture>]
    type Test() = class
    
        [<Test>]
        member self.testB001() =
            // operation: addition
            //
            //   f(x,y) = x + y
            //
            let f (x: bfloat) (y: bfloat): bfloat = x + y

            bfloat.CreateTape ()

            let x = bparam(1.45) :> bfloat
            let y = bparam(2.55) :> bfloat
            let h = f x y
            
            Assert.AreEqual(4.0, h.X, 1e-15, "f(x,y)")
            h.Diff ()
            Assert.AreEqual(1.0, x.D, 1e-15, "df/dx")
            Assert.AreEqual(1.0, y.D, 1e-15, "df/dy")
    
        [<Test>]
        member self.testB002() =
            // operation: subtraction
            //
            //   f(x,y) = x - y
            //
            let f (x: bfloat) (y: bfloat): bfloat = x - y

            bfloat.CreateTape ()

            let x = bparam(1.4) :> bfloat
            let y = bparam(2.5) :> bfloat
            let h = f x y
            
            Assert.AreEqual(-1.1, h.X, 1e-15, "f(x,y)")
            h.Diff ()
            Assert.AreEqual( 1.0, x.D, 1e-15, "df/dx")
            Assert.AreEqual(-1.0, y.D, 1e-15, "df/dy")
    
        [<Test>]
        member self.testB003() =
            // operation: multiplication
            //
            //   f(x,y) = x*y
            //
            let f (x: bfloat) (y: bfloat): bfloat = x*y

            bfloat.CreateTape ()

            let x = bparam(1.4) :> bfloat
            let y = bparam(2.5) :> bfloat
            let h = f x y
            
            Assert.AreEqual(3.5, h.X, 1e-15, "f(x,y)")
            h.Diff ()
            Assert.AreEqual(2.5, x.D, 1e-15, "df/dx")
            Assert.AreEqual(1.4, y.D, 1e-15, "df/dy")
    
        [<Test>]
        member self.testB004() =
            // operation: division
            //
            //   f(x,y) = x/y
            //
            let f (x: bfloat) (y: bfloat): bfloat = x/y

            bfloat.CreateTape ()

            let x = bparam(1.4) :> bfloat
            let y = bparam(2.5) :> bfloat
            let h = f x y
            
            Assert.AreEqual( 0.56,  h.X, 1e-15, "f(x,y)")
            h.Diff ()
            Assert.AreEqual( 0.4,   x.D, 1e-15, "df/dx")
            Assert.AreEqual(-0.224, y.D, 1e-15, "df/dy")
    
        [<Test>]
        member self.testB005() =
            // operation: exponentiation
            //
            //   f(x,y) = x**y
            //
            let f (x: bfloat) (y: bfloat): bfloat = x**y

            bfloat.CreateTape ()

            let x = bparam(1.4) :> bfloat
            let y = bparam(2.5) :> bfloat
            let h = f x y
            
            Assert.AreEqual(2.319103274975049,  h.X, 1e-15, "f(x,y)")
            h.Diff ()
            Assert.AreEqual(4.141255848169731,  x.D, 1e-15, "df/dx")
            Assert.AreEqual(0.7803138658864345, y.D, 1e-15, "df/dy")
    
        [<Test>]
        member self.testU001() =
            // operation: negation
            //
            //   f(x) = -x
            //
            let f (x: bfloat): bfloat = -x

            bfloat.CreateTape ()

            let x = bparam(1.4) :> bfloat
            let h = f x
            
            Assert.AreEqual(-1.4, h.X, 1e-15, "f(x)")
            h.Diff ()
            Assert.AreEqual(-1.0, x.D, 1e-15, "df/dx")
    
        [<Test>]
        member self.testU002() =
            // operation: sqrt
            //
            //   f(x) = sqrt(x)
            //
            let f (x: bfloat): bfloat = sqrt(x)

            bfloat.CreateTape ()

            let x = bparam(1.4) :> bfloat
            let h = f x
            
            Assert.AreEqual(1.1832159566199232, h.X, 1e-15, "f(x)")
            h.Diff ()
            Assert.AreEqual(0.4225771273642583, x.D, 1e-15, "df/dx")
    
        [<Test>]
        member self.testU003() =
            // operation: exp
            //
            //   f(x) = exp(x)
            //
            let f (x: bfloat): bfloat = exp(x)

            bfloat.CreateTape ()

            let x = bparam(1.4) :> bfloat
            let h = f x
            
            Assert.AreEqual(4.0551999668446745, h.X, 1e-15, "f(x)")
            h.Diff ()
            Assert.AreEqual(4.0551999668446745, x.D, 1e-15, "df/dx")
    
        [<Test>]
        member self.testU004() =
            // operation: log
            //
            //   f(x) = log(x)
            //
            let f (x: bfloat): bfloat = log(x)

            bfloat.CreateTape ()

            let x = bparam(1.4) :> bfloat
            let h = f x
            
            Assert.AreEqual(0.3364722366212129, h.X, 1e-15, "f(x)")
            h.Diff ()
            Assert.AreEqual(0.7142857142857143, x.D, 1e-15, "df/dx")
    
        [<Test>]
        member self.testF001() =
            // function:
            //
            //   f(x,y) = x^2/sqrt(y^2 - 2*y*x + x^2)
            //
            let f (x: bfloat) (y: bfloat): bfloat = x*x/sqrt(y*y - 2.0*x*y + x*x)

            bfloat.CreateTape ()

            let x = bparam(1.0) :> bfloat
            let y = bparam(2.0) :> bfloat
            let h = f x y
            
            Assert.AreEqual(h.X,  1.0, 1e-15)
            h.Diff 1.0
            Assert.AreEqual(x.D,  3.0, 1e-15)
            Assert.AreEqual(y.D, -1.0, 1e-15)
    
        [<Test>]
        member self.testF002() =
            // function:
            //
            //   f(x,y) = [x*sqrt(y); y/(sqrt(2)*x+1)]
            //
            let f (x: bfloat) (y: bfloat): bfloat*bfloat = (x*sqrt(y), y/(sqrt(2.0)*x+1.0))

            bfloat.CreateTape ()

            let x = bparam(3.0) :> bfloat
            let y = bparam(2.0) :> bfloat
            let h1, h2 = f x y
            
            Assert.AreEqual(h1.X, 4.242640687119286, 1e-15)
            Assert.AreEqual(h2.X, 0.381487139661092, 1e-15)

            h1.Diff 1.0
            Assert.AreEqual(x.D,  1.414213562373095, 1e-15)
            Assert.AreEqual(y.D,  1.060660171779821, 1e-15)
            h2.Diff 1.0
            Assert.AreEqual(x.D, -0.102906973599230, 1e-15)
            Assert.AreEqual(y.D,  0.190743569830546, 1e-15)
    
        [<Test>]
        member self.testF003() =
            // function:
            //
            //   f(k;x,y) = x**(k**y)
            //
//            let f (k: float) (x: bfloat) (y: bfloat): bfloat = x**(k**y)
            let f (k: float) (x: bfloat) (y: bfloat): bfloat = x**bfloat.Pow(k,y)

            // k = 1
            bfloat.CreateTape ()
            let x = bparam(0.2) :> bfloat
            let y = bparam(3.0) :> bfloat
            let p1 = f 1.0 x y
            Assert.AreEqual(0.2, p1.X, 1e-15, "f(x)")
            p1.Diff ()
            Assert.AreEqual(1.0, x.D, 1e-15, "df/dx")
            Assert.AreEqual(0.0, y.D, 1e-15, "df/dy")
            
            // k = 2
            bfloat.CreateTape ()
            let x = bparam(0.2) :> bfloat
            let y = bparam(3.0) :> bfloat
            let p2 = f 2.0 x y
            Assert.AreEqual(2.56000000000000000e-06, p2.X, 1e-15, "f(x)")
            p2.Diff ()
            Assert.AreEqual(0.000102400000000000000, x.D, 1e-15, "df/dx")
            Assert.AreEqual(-2.2847024154418818e-05, y.D, 1e-15, "df/dy")
    
        [<Test>]
        member self.testF004() =
            // function:
            //
            //   f(x) = x**1.5 + log(x)
            //
            let f (x: bfloat): bfloat = x**1.5 + log(x)

            bfloat.CreateTape ()
            let x = bparam(0.2) :> bfloat
            let p1 = f x
            Assert.AreEqual(-1.5199951933341087, p1.X, 1e-15, "f(x)")
            p1.Diff ()
            Assert.AreEqual( 5.670820393249937,  x.D,  1e-15, "df/dx")
    
        [<Test>]
        member self.testF005() =
            // function:
            //
            //   f(d; x) = log(x**d)
            //
            let f (d: bfloat) (x: bfloat): bfloat = log(x**d)

            bfloat.CreateTape ()
            let d = bparam(1.5)
            let x = bparam(0.2) :> bfloat
            let p1 = f d x
            Assert.AreEqual(-2.4141568686511503, p1.X, 1e-15, "f(x)")
            p1.Diff ()
            Assert.AreEqual( 7.5000000000000000, x.D,  1e-15, "df/dx")

        [<Test>]
        member self.testF006() =
            // function:
            //
            //   f1(x) = exp(log(x))
            //   f2(x) = log(exp(x))
            //
            let f1 (x: bfloat): bfloat = exp(log(x))
            let f2 (x: bfloat): bfloat = log(exp(x))

            let x = bparam(0.2) :> bfloat

            bfloat.CreateTape ()
            let p1 = f1 x
            Assert.AreEqual(0.2, p1.X, 1e-15, "f1(x)")
            p1.Diff ()
            Assert.AreEqual(1.0, x.D,  1e-15, "df1/dx")

            bfloat.CreateTape ()
            let p2 = f2 x
            Assert.AreEqual(0.2, p2.X, 1e-15, "f2(x)")
            p1.Diff ()
            Assert.AreEqual(1.0, x.D,  1e-15, "df2/dx")

        [<Test>]
        member self.testBiggsExp6() =
            // function: Biggs EXP6
            //
            // Biggs, M. C., Minimization algorithms making use of non-quadratic
            // properties of the objective function. J Inst. Math Appl 8:315-327,
            // 1971
            //
            // see also: http://www.mat.univie.ac.at/~neum/glopt/test.html
            //
            let f (x: bfloat[]): bfloat =
                let mutable f = bparam(0.0) :> bfloat
                for i in 1..13 do
                    let c = -float(i)/10.0
                    let fi =   x.[2]*exp(c*x.[0]) - x.[3]*exp(c*x.[1])
                             + x.[5]*exp(c*x.[4]) -       exp(c)
                             +   5.0*exp(10.0*c)  -   3.0*exp(4.0*c)
                    f <- f+ fi*fi
                f
            
            let g (x: float[]): float[] =
                let g = Array.create 6 0.0
                
                for i in 1..13 do
                    let c = -float(i)/10.0
                    let fi =   x.[2]*exp(c*x.[0]) - x.[3]*exp(c*x.[1])
                             + x.[5]*exp(c*x.[4]) -       exp(c)
                             +   5.0*exp(10.0*c)  -   3.0*exp(4.0*c)
                    
                    let dfdx0 =  c*x.[2]*exp(c*x.[0])
                    let dfdx1 = -c*x.[3]*exp(c*x.[1])
                    let dfdx2 =          exp(c*x.[0])
                    let dfdx3 = -        exp(c*x.[1])
                    let dfdx4 =  c*x.[5]*exp(c*x.[4])
                    let dfdx5 =          exp(c*x.[4])
    
                    g.[0] <- g.[0] + 2.0*fi*dfdx0
                    g.[1] <- g.[1] + 2.0*fi*dfdx1
                    g.[2] <- g.[2] + 2.0*fi*dfdx2
                    g.[3] <- g.[3] + 2.0*fi*dfdx3
                    g.[4] <- g.[4] + 2.0*fi*dfdx4
                    g.[5] <- g.[5] + 2.0*fi*dfdx5
                g
            
            bfloat.CreateTape ()

            let x = [| bparam(1.0) :> bfloat;
                       bparam(1.0) :> bfloat;
                       bparam(1.0) :> bfloat;
                       bparam(1.0) :> bfloat;
                       bparam(1.0) :> bfloat;
                       bparam(1.0) :> bfloat |]

            let h = f x
            let dh = g [| x.[0].X; x.[1].X; x.[2].X; x.[3].X; x.[4].X; x.[5].X |]
            
            h.Diff ()
            Assert.AreEqual(dh.[0], x.[0].D, 1e-15)
            Assert.AreEqual(dh.[1], x.[1].D, 1e-15)
            Assert.AreEqual(dh.[2], x.[2].D, 1e-15)
            Assert.AreEqual(dh.[3], x.[3].D, 1e-15)
            Assert.AreEqual(dh.[4], x.[4].D, 1e-15)
            Assert.AreEqual(dh.[5], x.[5].D, 1e-15)
    
        [<Test>]
        member self.testOsborneII() =
            // function: Osborne II
            //
            // see also: http://www.mat.univie.ac.at/~neum/glopt/test.html
            //
            let m = 65
            let n = 11
            let y = [| 1.366;  1.191;  1.112;  1.013;  0.991;
                       0.885;  0.831;  0.847;  0.786;  0.725;
                       0.746;  0.679;  0.608;  0.655;  0.616;
                       0.606;  0.602;  0.626;  0.651;  0.724;
                       0.649;  0.649;  0.694;  0.644;  0.624;
                       0.661;  0.612;  0.558;  0.533;  0.495;
                       0.500;  0.423;  0.395;  0.375;  0.372;
                       0.391;  0.396;  0.405;  0.428;  0.429;
                       0.523;  0.562;  0.607;  0.653;  0.672;
                       0.708;  0.633;  0.668;  0.645;  0.632;
                       0.591;  0.559;  0.597;  0.625;  0.739;
                       0.710;  0.729;  0.720;  0.636;  0.581;
                       0.428;  0.292;  0.162;  0.098;  0.054|]
            let osbII (x: float[]) (f: float[]) (J1: float[,]) (J2: float[,]) (i: int) =
                async {
                    bfloat.CreateTape ()
                    
                    let x = Array.map (fun v -> bparam(v) :> bfloat) x
                    
                    let t = float(i)/10.0
                    let t09 = t-x.[8]
                    let t10 = t-x.[9]
                    let t11 = t-x.[10]
                    let s09 = t09*t09 // t09**2
                    let s10 = t10*t10 // t10**2
                    let s11 = t11*t11 // t11**2
                    let e1 = exp(  -t*x.[4])
                    let e2 = exp(-s09*x.[5])
                    let e3 = exp(-s10*x.[6])
                    let e4 = exp(-s11*x.[7])
                    
                    let fi = (x.[0]*e1 + x.[1]*e2 + x.[2]*e3 + x.[3]*e4) - y.[i]
        
                    let r2 = x.[1].X*e2.X
                    let r3 = x.[2].X*e3.X
                    let r4 = x.[3].X*e4.X
            
                    J1.[i, 0] <- e1.X
                    J1.[i, 1] <- e2.X
                    J1.[i, 2] <- e3.X
                    J1.[i, 3] <- e4.X
                    J1.[i, 4] <- -t*x.[0].X*e1.X
                    J1.[i, 5] <- -s09.X*r2
                    J1.[i, 6] <- -s10.X*r3
                    J1.[i, 7] <- -s11.X*r4
                    J1.[i, 8] <- 2.0*t09.X*x.[5].X*r2
                    J1.[i, 9] <- 2.0*t10.X*x.[6].X*r3
                    J1.[i,10] <- 2.0*t11.X*x.[7].X*r4
                    
                    f.[i] <- fi.X
                    fi.Diff ()
                    for j in 0..n-1 do
                        J2.[i,j] <- x.[j].D
                }

            let x = [| 1.3100;
                       0.4315;
                       0.6336;
                       0.5993;
                       0.7539;
                       0.9056;
                       1.3651;
                       4.8248;
                       2.3988;
                       4.5689;
                       5.6754 |]
            let f = Array.create m 0.0
            let J1 = Array2D.create m n 0.0
            let J2 = Array2D.create m n 0.0
            
            seq { 0 .. m-1 }
            |> Seq.map (osbII x f J1 J2)
            |> Async.Parallel 
            |> Async.RunSynchronously
            |> ignore
            
            for i in 0..m-1 do
                for j in 0..n-1 do
                    Assert.AreEqual(J1.[i,j], J2.[i,j], 1e-15, sprintf "J[%d,%d]" i  j)
    end

