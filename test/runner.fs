
open System;
open System.Reflection;

open NUnit.Common;
open NUnitLite;

[<EntryPoint>]
let main args =
    let reader = Console.In
    use writer = new ExtendedTextWrapper(Console.Out)
    AutoRun(Assembly.GetExecutingAssembly()).Execute(args, writer, reader);
