namespace adfquick

type IEnumerable = System.Collections.IEnumerable
type IEnumerable<'T> = System.Collections.Generic.IEnumerable<'T>
type LinkedList<'T> = System.Collections.Generic.LinkedList<'T>

[<AllowNullLiteral>]
type Tape internal () =

    static let ChunkSize = 8192

    let mutable idx: int = ChunkSize
    let chunk: LinkedList<bfloat[]> = new LinkedList<bfloat[]>()
    
    member t.Push (b: bfloat) =
        if idx = ChunkSize then
            chunk.AddFirst (Array.zeroCreate ChunkSize) |> ignore
            idx <- 0
        chunk.First.Value.[idx] <- b
        idx <- idx + 1
    
    member private t.toSeq () =
        seq {
            for c in chunk do
                let n = if c = chunk.First.Value then idx-1 else ChunkSize-1
                for i in n .. -1 .. 0 do
                    yield c.[i]
        }
    
    interface IEnumerable<bfloat> with
        member s.GetEnumerator () = s.toSeq().GetEnumerator ()

    interface IEnumerable with
        member s.GetEnumerator () = s.toSeq().GetEnumerator () :> System.Collections.IEnumerator

and [<AbstractClass>] bfloat =

    [<DefaultValue>]
    [<System.ThreadStatic>]
    static val mutable private tape: Tape

    val internal xval: float
    val mutable internal dval: float
    
    internal new (x: float) as bf =
        { xval = x; dval = 0.0 }
        then
            if bfloat.tape = null then
                bfloat.tape <- Tape()
            bfloat.tape.Push(bf)
    internal new () = bfloat(0.0)

    member bf.X with get() = bf.xval
    member bf.D with get() = bf.dval
    
    abstract member Calc: unit -> unit
    
    member bf.Diff (): unit =
        bf.Diff (1.0)
    
    member bf.Diff (y: float): unit =
        Seq.iter (fun (b: bfloat) -> b.dval <- 0.0) bfloat.tape
        bf.dval <- y
        Seq.iter (fun (b: bfloat) -> b.Calc ()) bfloat.tape
    
    static member CreateTape () =
        bfloat.tape <- Tape()
    
    static member Tape
        with get() = bfloat.tape
        and  set(t: Tape) = bfloat.tape <- t
    
    static member (+) (x: bfloat, y: bfloat) =
        bfloat_add(x, y) :> bfloat
    
    static member (+) (x: float, y: bfloat) =
        bfloat_cadd(x, y) :> bfloat
    
    static member (+) (x: bfloat, y: float) =
        bfloat_cadd(y, x) :> bfloat
    
    static member (-) (x: bfloat, y: bfloat) =
        bfloat_sub(x, y) :> bfloat
    
    static member (-) (x: float, y: bfloat) =
        bfloat_csub(x, y) :> bfloat
    
    static member (-) (x: bfloat, y: float) =
        bfloat_cadd(-y, x) :> bfloat
    
    static member ( * ) (x: bfloat, y: bfloat) =
        bfloat_mul(x, y) :> bfloat
    
    static member ( * ) (x: float, y: bfloat) =
        bfloat_cmul(x, y) :> bfloat
    
    static member ( * ) (x: bfloat, y: float) =
        bfloat_cmul(y, x) :> bfloat
    
    static member (/) (x: bfloat, y: bfloat) =
        bfloat_div(x, y) :> bfloat
    
    static member (/) (x: float, y: bfloat) =
        bfloat_cdiv(x, y) :> bfloat
    
    static member (/) (x: bfloat, y: float) =
        bfloat_cmul(1.0/y, x) :> bfloat
    
    static member Pow (x: bfloat, y: float) =
        bfloat_bpow(x, y) :> bfloat

    static member Pow (x: float, y: bfloat) =
        bfloat_epow(x, y) :> bfloat

    static member Pow (x: bfloat, y: bfloat) =
        bfloat_pow(x, y) :> bfloat

    static member (~-) (x: bfloat) =
        bfloat_neg(x) :> bfloat
    
    static member Sqrt (x: bfloat) =
        bfloat_sqrt(x) :> bfloat

    static member Exp (x: bfloat) =
        bfloat_exp(x) :> bfloat

    static member Log (x: bfloat) =
        bfloat_log(x) :> bfloat

    static member Zero: bfloat = bparam(0.0) :> bfloat

    static member One: bfloat = bparam(1.0) :> bfloat
    
    interface System.IComparable with
        member x.CompareTo(y: obj) = 
            match y with
            | :? bfloat -> compare x.X (y:?>bfloat).X
            | :? float  -> compare x.X (y:?>float)
            | _ -> failwith "Unsupported type for comparison."
    end

    override bf.Equals(o: obj) =
        match o with
        | :? bfloat as bo -> bf.X = bo.X
        | _ -> false

    override bf.GetHashCode() = bf.X.GetHashCode()

//    interface System.IComparable<bfloat> with
//        member x.CompareTo(y: bfloat) = compare x.X y.X
//    end
    
//    interface System.IComparable<float> with
//        member x.CompareTo(y: float) = compare x.X y
//    end

and bparam(c: float) =
    inherit bfloat(c)
    override bf.Calc () = ()

and internal bfloat_add(l: bfloat, r: bfloat) =
    inherit bfloat(l.X+r.X)
    
    override bf.Calc () =
        l.dval <- l.dval + bf.dval
        r.dval <- r.dval + bf.dval

and internal bfloat_cadd(c: float, x: bfloat) =
    inherit bfloat(c+x.X)
    
    override bf.Calc () =
        x.dval <- x.dval + bf.dval

and internal bfloat_sub(l: bfloat, r: bfloat) =
    inherit bfloat(l.X-r.X)
    
    override bf.Calc () =
        l.dval <- l.dval + bf.dval
        r.dval <- r.dval - bf.dval

and internal bfloat_csub(l: float, r: bfloat) =
    inherit bfloat(l-r.X)
    
    override bf.Calc () =
        r.dval <- r.dval - bf.dval

and internal bfloat_mul(l: bfloat, r: bfloat) =
    inherit bfloat(l.X*r.X)
    
    override bf.Calc () =
        l.dval <- l.dval + bf.dval*r.xval
        r.dval <- r.dval + bf.dval*l.xval

and internal bfloat_cmul(c: float, x: bfloat) =
    inherit bfloat(c*x.X)
    
    override bf.Calc () =
        x.dval <- x.dval + bf.dval*c

and internal bfloat_div(l: bfloat, r: bfloat) =
    inherit bfloat(l.X/r.X)
    
    override bf.Calc () =
        let ddr = bf.dval/r.xval
        l.dval <- l.dval + ddr
        r.dval <- r.dval - ddr*bf.xval

and internal bfloat_cdiv(l: float, r: bfloat) =
    inherit bfloat(l/r.X)
    
    override bf.Calc () =
        let ddr = bf.dval/r.xval
        r.dval <- r.dval - ddr*bf.xval

and internal bfloat_pow(a: bfloat, b: bfloat) =
    inherit bfloat(a.X**b.X)
    
    override bf.Calc () =
        a.dval <- a.dval + bf.dval*b.xval*(a.xval**(b.xval-1.0))
        b.dval <- b.dval + bf.dval*bf.xval*log(a.xval)

and internal bfloat_bpow(a: bfloat, b: float) =
    inherit bfloat(a.X**b)
    
    override bf.Calc () =
        a.dval <- a.dval + bf.dval*b*(a.xval**(b-1.0))

and internal bfloat_epow(a: float, b: bfloat) =
    inherit bfloat(a**b.X)
    
    override bf.Calc () =
        b.dval <- b.dval + bf.dval*bf.xval*log(a)

and internal bfloat_neg(x: bfloat) =
    inherit bfloat(-x.X)
    
    override bf.Calc () =
        x.dval <- x.dval - bf.dval

and internal bfloat_sqrt(x: bfloat) =
    inherit bfloat(sqrt(x.X))
    
    override bf.Calc () =
        x.dval <- x.dval + bf.dval/bf.xval/2.0

and internal bfloat_exp(x: bfloat) =
    inherit bfloat(exp(x.X))
    
    override bf.Calc () =
        x.dval <- x.dval + bf.dval*bf.xval

and internal bfloat_log(x: bfloat) =
    inherit bfloat(log(x.X))
    
    override bf.Calc () =
        x.dval <- x.dval + bf.dval/x.xval


